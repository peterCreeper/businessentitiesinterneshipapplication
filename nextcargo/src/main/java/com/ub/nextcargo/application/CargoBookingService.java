package com.ub.nextcargo.application;

import com.ub.nextcargo.domen.Cargo;
import com.ub.nextcargo.domen.Customer;
import com.ub.nextcargo.domen.Itinerary;
import com.ub.nextcargo.util.Location;
import org.springframework.stereotype.Service;

public interface CargoBookingService {

    public Cargo bookNewCargo(Cargo cargo);

    public void attachItineraryToCargo(Cargo cargo, Itinerary itinerary);

    public void changeDestination(Cargo cargo, Location newDestination) ;
}
