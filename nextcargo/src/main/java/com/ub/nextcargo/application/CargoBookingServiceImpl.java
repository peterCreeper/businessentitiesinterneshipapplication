package com.ub.nextcargo.application;

import com.ub.nextcargo.domen.Cargo;
import com.ub.nextcargo.domen.Customer;
import com.ub.nextcargo.domen.Itinerary;
import com.ub.nextcargo.exceptions.NoSuchCargoException;
import com.ub.nextcargo.persistance.CargoRepository;
import com.ub.nextcargo.util.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

@Service("cargoBookingService")
public class CargoBookingServiceImpl implements CargoBookingService {

    private CargoRepository cargoRepository;

    public CargoBookingServiceImpl(CargoRepository cargoRepository) {
        this.cargoRepository = cargoRepository;
    }

    @Override
    public Cargo bookNewCargo(Cargo cargo) {
        return cargoRepository.save(cargo);
    }

    @Override
    public void attachItineraryToCargo(Cargo cargo, Itinerary itinerary) {
        cargo.setNewItinerary(itinerary);
    }

    @Override
    public void changeDestination(Cargo cargo, Location newDestination) {
        cargo.defineNewRoute(cargo.getOrigin(), newDestination, cargo.getOriginDate(), cargo.getDestinationDate());
    }
}
