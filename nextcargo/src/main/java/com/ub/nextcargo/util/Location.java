package com.ub.nextcargo.util;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

public final class Location {

    private final String country;
    private final String area;
    private final String city;
    private final String district;
    private final String street;
    private final String house;
    private final String apartment;

    public String getCountry() {
        return country;
    }

    public String getArea() {
        return area;
    }

    public String getCity() {
        return city;
    }

    public String getDistrict() {
        return district;
    }

    public String getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    public String getApartment() {
        return apartment;
    }

    public Location(String country, String area, String city, String district, String street, String house, String apartment) {
        this.country = country;
        this.area = area;
        this.city = city;
        this.district = district;
        this.street = street;
        this.house = house;
        this.apartment = apartment;
    }

    public boolean isValid() {
        return country != null && area != null && city != null && district != null && street != null && house != null && apartment != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Objects.equals(country, location.country) &&
                Objects.equals(area, location.area) &&
                Objects.equals(city, location.city) &&
                Objects.equals(district, location.district) &&
                Objects.equals(street, location.street) &&
                Objects.equals(house, location.house) &&
                Objects.equals(apartment, location.apartment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country, area, city, district, street, house, apartment);
    }

    @Override
    public String toString() {
        return String.format("%s\\%s\\%s\\%s\\%s\\%s\\%s", country, area, city, district, street, house, apartment);
    }
}
