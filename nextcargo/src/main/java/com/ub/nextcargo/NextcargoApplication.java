package com.ub.nextcargo;

import com.ub.nextcargo.application.CargoBookingService;
import com.ub.nextcargo.domen.Cargo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class NextcargoApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext appContext = SpringApplication.run(NextcargoApplication.class, args);

    }
}
