package com.ub.nextcargo.exceptions;

public class OriginWasEqualToDestinationException extends RuntimeException {
    public OriginWasEqualToDestinationException(String s) {
        super(s);
    }
}
