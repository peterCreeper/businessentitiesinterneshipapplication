package com.ub.nextcargo.exceptions;

public class NoSuchCargoException extends RuntimeException{
    public NoSuchCargoException(){
        super();
    }

    public NoSuchCargoException(String msg){
        super(msg);
    }
}
