package com.ub.nextcargo.exceptions;

public class CargoFieldWasNullException extends InvalidCargoException {
    public CargoFieldWasNullException(String s){
        super(s);
    }
}
