package com.ub.nextcargo.exceptions;

public class InvalidCargoException extends RuntimeException {
    public InvalidCargoException(String s) {
        super(s);
    }
}
