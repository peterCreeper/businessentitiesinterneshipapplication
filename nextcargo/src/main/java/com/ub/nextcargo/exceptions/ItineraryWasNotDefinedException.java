package com.ub.nextcargo.exceptions;

public class ItineraryWasNotDefinedException extends InvalidCargoException{
    public ItineraryWasNotDefinedException(String s) {
        super(s);
    }
}
