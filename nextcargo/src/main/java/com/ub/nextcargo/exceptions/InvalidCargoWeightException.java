package com.ub.nextcargo.exceptions;

public class InvalidCargoWeightException extends InvalidCargoException{
    public InvalidCargoWeightException(String s) {
        super(s);
    }
}
