package com.ub.nextcargo.persistance;

import com.ub.nextcargo.domen.Customer;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.List;

@Component
public class CustomerRepositorySeeker {//implements CommandLineRunner {

    private CustomerRepository customerRepository;

    public CustomerRepositorySeeker(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> findAll(){
        return customerRepository.findAll();
    }

    //@Override
    //public void run(String... args) throws Exception {
    //    customerRepository.deleteAll();
    //    Customer c1 = new Customer("Peter", "Shitter", "HolyFucker", LocalDate.now());
    //    Customer c2 = new Customer("Emely", "Baca", "Sbiet", LocalDate.now());
    //    Customer c3 = new Customer("Guss", "Puss", "OhMine", LocalDate.now());
    //    Customer c4 = new Customer("Zelinskiy", "Sasha", "Konstantiniwhich", LocalDate.now());
    //    customerRepository.save(c1);
    //    customerRepository.save(c2);
    //    customerRepository.save(c3);
    //    customerRepository.save(c4);
    //}
}
