package com.ub.nextcargo.persistance;

import com.ub.nextcargo.domen.Customer;
import com.ub.nextcargo.domen.Schedule;
import com.ub.nextcargo.domen.Voyage;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VoyageRepositorySeeker {//implements CommandLineRunner {

    private VoyageRepository voyageRepository;

    public VoyageRepositorySeeker(VoyageRepository customerRepository) {
        this.voyageRepository = customerRepository;
    }

    public List<Voyage> findAll(){
        return voyageRepository.findAll();
    }

    //@Override
    //public void run(String... args) throws Exception {
    //    voyageRepository.deleteAll();
    //
    //    Voyage v1 = new Voyage("1a", null);
    //    Voyage v2 = new Voyage("2a", null);
    //
    //    voyageRepository.save(v1);
    //    voyageRepository.save(v2);
    //}
}
