package com.ub.nextcargo.persistance;

import com.ub.nextcargo.domen.Voyage;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoyageRepository extends MongoRepository<Voyage, ObjectId> {
}
