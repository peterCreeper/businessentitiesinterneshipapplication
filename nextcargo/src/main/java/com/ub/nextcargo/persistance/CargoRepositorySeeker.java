package com.ub.nextcargo.persistance;

import com.ub.nextcargo.domen.*;
import com.ub.nextcargo.util.Location;
import org.bson.types.ObjectId;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import static org.springframework.data.mongodb.core.query.Update.update;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@Component
public class CargoRepositorySeeker {

    private CargoRepository cargoRepository;
    private MongoTemplate mongoTemplate;

    public CargoRepositorySeeker(CargoRepository cargoRepository, MongoTemplate mongoTemplate) {
        this.cargoRepository = cargoRepository;
        this.mongoTemplate = mongoTemplate;
    }

    public Page<Cargo> findAll(Pageable pageable) {
        return cargoRepository.findAll(pageable);
    }

    public Cargo findById(ObjectId id) {
        Optional<Cargo> optionalCargo = cargoRepository.findById(id);
        return optionalCargo.orElse(null);
    }

    public Cargo replaceDocument(ObjectId id, Cargo newCargo) {
        return mongoTemplate.findAndReplace(Query.query(Criteria.where("id").is(id)), newCargo);
    }

    //@Override
    //public void run(String... args) throws Exception {
    //    Location location1 = new Location("a", "b", "s", "s", "v", "v", "1");
    //    Location location2 = new Location("dfgfdgf", "b", "s", "s", "v", "v", "1");
    //    updateValue(new ObjectId("5f5b4477b1386e7dc57206df"), new Cargo(location1, location2, LocalDate.now(), LocalDate.now(), 69));
    //}
}
