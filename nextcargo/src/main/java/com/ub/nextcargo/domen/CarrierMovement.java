package com.ub.nextcargo.domen;

import com.ub.nextcargo.util.Location;

import java.time.LocalDateTime;

public final class CarrierMovement {

    private final Location departureLocation;
    private final Location arrivalLocation;
    private final LocalDateTime departureTime;
    private final LocalDateTime arrivalTime;

    public CarrierMovement(Location departureLocation, Location arrivalLocation, LocalDateTime departureTime, LocalDateTime arrivalTime) {
        this.departureLocation = departureLocation;
        this.arrivalLocation = arrivalLocation;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
    }

    public Location getDepartureLocation() {
        return departureLocation;
    }

    public Location getArrivalLocation() {
        return arrivalLocation;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    @Override
    public String toString() {
        return "CarrierMovement{" +
                "departureLocation=" + departureLocation +
                ", arrivalLocation=" + arrivalLocation +
                ", departureTime=" + departureTime +
                ", arrivalTime=" + arrivalTime +
                '}';
    }
}
