package com.ub.nextcargo.domen;

import com.ub.nextcargo.exceptions.OriginWasEqualToDestinationException;
import com.ub.nextcargo.util.Location;
import org.springframework.data.annotation.Immutable;
import org.springframework.data.annotation.PersistenceConstructor;

import java.time.LocalDateTime;
import java.util.List;

public final class Delivery {

    public enum TransportationStatus {
        NOT_RECEIVED,
        IN_PORT,
        ON_BOARD,
        CLAIMED,
        UNKNOWN
    }

    public enum RoutingStatus {
        NOT_ROUTED,
        ROUTED,
        MISROUTED
    }

    private final Voyage currentVoyage;
    private final Location lastKnownLocation;
    private final TransportationStatus transportationStatus;
    private final RoutingStatus routingStatus;
    private final LocalDateTime calculatedAt;

    public Voyage getCurrentVoyage() {
        return currentVoyage;
    }

    public Location getLastKnownLocation() {
        return lastKnownLocation;
    }

    public TransportationStatus getTransportationStatus() {
        return transportationStatus;
    }

    public RoutingStatus getRoutingStatus() {
        return routingStatus;
    }

    public Delivery(Itinerary itinerary, Location origin, Location destination) {

        List<RouteStage> routeStages = itinerary.getRouteStages();

        if (!routeStages.isEmpty()) {
            RouteStage firstRouteStage = routeStages.get(0);

            currentVoyage = firstRouteStage.getVoyage();
            lastKnownLocation = firstRouteStage.getLoadLocation();

        } else {
            currentVoyage = null;
            lastKnownLocation = null;
        }

        if (!destination.equals(origin))
            routingStatus = RoutingStatus.ROUTED;
        else
            throw new OriginWasEqualToDestinationException("destination was equal to origin");

        calculatedAt = LocalDateTime.now();

        transportationStatus = TransportationStatus.NOT_RECEIVED;
    }

    @PersistenceConstructor
    private Delivery(Voyage currentVoyage,
                     Location lastKnownLocation,
                     TransportationStatus transportationStatus,
                     RoutingStatus routingStatus,
                     LocalDateTime calculatedAt) {
        this.currentVoyage = currentVoyage;
        this.lastKnownLocation = lastKnownLocation;
        this.transportationStatus = transportationStatus;
        this.routingStatus = routingStatus;
        this.calculatedAt = calculatedAt;
    }

    public boolean isDetermined() {
        return currentVoyage != null && lastKnownLocation != null;
    }
}
