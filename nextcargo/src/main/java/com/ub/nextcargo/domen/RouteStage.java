package com.ub.nextcargo.domen;

import com.ub.nextcargo.util.Location;
import org.springframework.data.annotation.PersistenceConstructor;

import java.time.LocalDateTime;

public final class RouteStage {

    private final Voyage voyage;
    private final Location loadLocation;
    private final Location unloadLocation;
    private final LocalDateTime loadTime;
    private final LocalDateTime unloadTime;

    @PersistenceConstructor
    public RouteStage(Voyage voyage,
                      Location loadLocation,
                      Location unloadLocation,
                      LocalDateTime loadTime,
                      LocalDateTime unloadTime) {
        this.voyage = voyage;
        this.loadLocation = loadLocation;
        this.unloadLocation = unloadLocation;
        this.loadTime = loadTime;
        this.unloadTime = unloadTime;
    }

    public Location getUnloadLocation() {
        return unloadLocation;
    }

    public LocalDateTime getLoadTime() {
        return loadTime;
    }

    public LocalDateTime getUnloadTime() {
        return unloadTime;
    }

    public Voyage getVoyage(){
        return voyage;
    }

    public Location getLoadLocation(){
        return loadLocation;
    }
}
