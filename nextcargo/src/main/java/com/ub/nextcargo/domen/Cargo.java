package com.ub.nextcargo.domen;

import com.ub.nextcargo.exceptions.*;
import com.ub.nextcargo.util.Location;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Document
public class Cargo {
    @Id
    private ObjectId id;

    private Customer customer;

    private Location origin;
    private Location destination;

    private LocalDate originDate;
    private LocalDate destinationDate;

    private double weight;
    private Itinerary itinerary;
    private Delivery delivery;

    public Cargo() {
        customer = null;
        itinerary = new Itinerary();
    }

    protected Cargo(Customer customer, Location origin, Location destination, LocalDate originDate, LocalDate destinationDate, double weight, Itinerary itinerary, Delivery delivery) {
        this.customer = customer;
        this.origin = origin;
        this.destination = destination;
        this.originDate = originDate;
        this.destinationDate = destinationDate;
        this.weight = weight;
        this.itinerary = itinerary;
        this.delivery = delivery;
    }

    public Cargo(Location origin,
                 Location destination,
                 LocalDate originDate,
                 LocalDate destinationDate,
                 double weight) {
        this();

        this.origin = origin;
        this.destination = destination;
        this.originDate = originDate;
        this.destinationDate = destinationDate;
        this.weight = weight;

        validate();

        delivery = new Delivery(itinerary, origin, destination);
    }

    public void defineNewRoute(Location newOrigin, Location newDestination, LocalDate newOriginDate, LocalDate newDestinationDate) {

        origin = newOrigin;
        destination = newDestination;
        originDate = newOriginDate;
        destinationDate = newDestinationDate;

        validate();

        if (itinerary.isDetermined())
            delivery = new Delivery(itinerary, origin, destination);
        else throw new ItineraryWasNotDefinedException("itinerary was not defined");
    }

    public void setNewItinerary(Itinerary newItinerary) {
        if (newItinerary == null) throw new CargoFieldWasNullException("itinerary was null");
        itinerary = newItinerary;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    private void validate() {

        if (origin == null)
            throw new CargoFieldWasNullException("origin was null");

        if (destination == null)
            throw new CargoFieldWasNullException("destination was null");

        if (originDate == null)
            throw new CargoFieldWasNullException("originDate was null");

        if (destinationDate == null)
            throw new CargoFieldWasNullException("destinationDate was null");

        if (weight <= 0)
            throw new InvalidCargoWeightException("weight was 0 or negative");

        if (origin.equals(destination))
            throw new OriginWasEqualToDestinationException("");
    }

    public ObjectId getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Location getOrigin() {
        return origin;
    }

    public Location getDestination() {
        return destination;
    }

    public LocalDate getOriginDate() {
        return originDate;
    }

    public LocalDate getDestinationDate() {
        return destinationDate;
    }

    public double getWeight() {
        return weight;
    }

    public Itinerary getItinerary() {
        return itinerary;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    @Override
    public String toString() {
        return "Cargo{" +
                "id=" + id +
                ", customer=" + customer +
                ", origin=" + origin +
                ", destination=" + destination +
                ", originData=" + originDate +
                ", destinationDate=" + destinationDate +
                ", weight=" + weight +
                ", itinerary=" + itinerary +
                ", delivery=" + delivery +
                '}';
    }
}