package com.ub.nextcargo.domen;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document
public class Voyage {
    @Id
    private ObjectId id;

    @Indexed
    private String number;
    private Schedule schedule;

    public ObjectId getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    @PersistenceConstructor
    public Voyage(String number, Schedule schedule) {
        this.number = number;
        this.schedule = schedule;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    @Override
    public String toString() {
        return "Voyage{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", schedule=" + schedule +
                '}';
    }
}
