package com.ub.nextcargo.domen;

import org.springframework.data.annotation.PersistenceConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class Schedule {
    private final List<CarrierMovement> carrierMovements;

    public List<CarrierMovement> getCarrierMovements() {
        return Collections.unmodifiableList(carrierMovements);
    }

    @PersistenceConstructor
    public Schedule(List<CarrierMovement> carrierMovements) {
        if (carrierMovements != null)
            this.carrierMovements = new ArrayList<>(carrierMovements);
        else
            this.carrierMovements = new ArrayList<>();
    }

    public Schedule() {
        carrierMovements = new ArrayList<>();
    }
}
