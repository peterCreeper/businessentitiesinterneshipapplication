package com.ub.nextcargo.domen;


import org.springframework.data.annotation.Immutable;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class Itinerary {
    private final List<RouteStage> routeStages;

    public Itinerary() {
        this.routeStages = new ArrayList<>();
    }

    @PersistenceConstructor
    public Itinerary(ArrayList<RouteStage> routeStages) {
        if (routeStages != null)
            this.routeStages = new ArrayList<>(routeStages);
        else
            this.routeStages = new ArrayList<>();
    }

    public boolean isDetermined() {
        return !routeStages.isEmpty();
    }

    public List<RouteStage> getRouteStages() {
        return Collections.unmodifiableList(routeStages);
    }

    @Override
    public String toString() {
        return "Itinerary{" +
                "routeStages=" + Arrays.toString(routeStages.toArray()) +
                '}';
    }
}
