package com.ub.nextcargo.interfaces.dto;

import com.ub.nextcargo.domen.Cargo;
import com.ub.nextcargo.util.Location;

import java.time.LocalDate;

public class CargoDto {
    private Location origin;
    private Location destination;
    private LocalDate originDate;
    private LocalDate destinationDate;
    private double weight;

    public CargoDto(Cargo cargo) {
        origin = cargo.getOrigin();
        destination = cargo.getDestination();
        originDate = cargo.getOriginDate();
        destinationDate = cargo.getDestinationDate();
        weight = cargo.getWeight();
    }

    public Location getOrigin() {
        return origin;
    }

    public Location getDestination() {
        return destination;
    }

    public LocalDate getOriginDate() {
        return originDate;
    }

    public LocalDate getDestinationDate() {
        return destinationDate;
    }

    public double getWeight() {
        return weight;
    }
}
