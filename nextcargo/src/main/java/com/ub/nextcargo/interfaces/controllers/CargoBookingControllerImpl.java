package com.ub.nextcargo.interfaces.controllers;

import com.ub.nextcargo.application.CargoBookingService;
import com.ub.nextcargo.domen.Cargo;
import com.ub.nextcargo.domen.Customer;
import com.ub.nextcargo.domen.Itinerary;
import com.ub.nextcargo.exceptions.CargoFieldWasNullException;
import com.ub.nextcargo.exceptions.OriginWasEqualToDestinationException;
import com.ub.nextcargo.interfaces.dto.CargoBookingRequest;
import com.ub.nextcargo.interfaces.dto.CargoDto;
import com.ub.nextcargo.persistance.CargoRepositorySeeker;
import com.ub.nextcargo.util.Location;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

// documentation access:  ->  http://localhost:8080/swagger-ui/

@RestController
@RequestMapping("/api")
class CargoBookingControllerImpl {

    CargoRepositorySeeker cargoRepositorySeeker;
    CargoBookingService cargoBookingService;

    public CargoBookingControllerImpl(CargoRepositorySeeker cargoRepositorySeeker, CargoBookingService cargoBookingService) {
        this.cargoRepositorySeeker = cargoRepositorySeeker;
        this.cargoBookingService = cargoBookingService;
    }

    @GetMapping(value = "/v1/cargos")
    public Page<CargoDto> getAll(@PageableDefault(sort = {"destinationDate"}, direction = Sort.Direction.DESC) Pageable pageable) {

        Page<Cargo> pageOfCargos = cargoRepositorySeeker.findAll(pageable);

        return pageOfCargos.map(CargoDto::new);
    }

    @PostMapping(value = "/v1/cargos")
    public ResponseEntity<ObjectId> bookCargo(@RequestBody CargoBookingRequest request) {
        Cargo cargo = new Cargo(request.getOrigin(), request.getDestination(), LocalDate.now(), LocalDate.now().plusDays(3), request.getWeight());
        Customer customer = new Customer(request.getLastName(), request.getFirstName(), request.getMiddleName(), request.getBirthDay());

        cargoBookingService.bookNewCargo(cargo);

        return new ResponseEntity<>(cargo.getId(), HttpStatus.OK);
    }

    @PostMapping(value = "/v1/cargos/itineraries/{id}")
    public ResponseEntity<ObjectId> chooseItinerary(@RequestBody Itinerary itinerary, @PathVariable ObjectId id) {

        if (!itinerary.isDetermined()) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        Cargo cargo = cargoRepositorySeeker.findById(id);

        if (cargo == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        cargoBookingService.attachItineraryToCargo(cargo, itinerary);

        if (cargoRepositorySeeker.replaceDocument(id, cargo) != null) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping(value = "/v1/cargos/destination/{id}")
    public ResponseEntity<ObjectId> changeDestinationOfCargo(@RequestBody Location destination, @PathVariable ObjectId id) {

        if (!destination.isValid())
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);

        Cargo cargo = cargoRepositorySeeker.findById(id);

        if (cargo == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        try {
            cargoBookingService.changeDestination(cargo, destination);
        } catch (CargoFieldWasNullException cargoFieldWasNullException) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (OriginWasEqualToDestinationException originWasEqualToDestinationException) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        if (cargoRepositorySeeker.replaceDocument(id, cargo) != null) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/v1/cargos/{id}")
    public ResponseEntity<CargoDto> getInfoById(@PathVariable ObjectId id) {
        Cargo cargo = cargoRepositorySeeker.findById(id);
        if (cargo == null) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        CargoDto cargoDto = new CargoDto(cargo);
        return new ResponseEntity<>(cargoDto, HttpStatus.OK);
    }
}
