package com.ub.nextcargo.interfaces.dto;

import com.ub.nextcargo.domen.Cargo;
import com.ub.nextcargo.domen.Customer;
import com.ub.nextcargo.util.Location;

import java.time.LocalDate;

//  параметры который задаёт юзер при создании груза
public class CargoBookingRequest {

    private Location origin;
    private Location destination;
    private double weight;
    private String lastName;
    String firstName;
    String middleName;
    LocalDate birthDay;

    public CargoBookingRequest(Location origin, Location destination, double weight, String lastName, String firstName, String middleName, LocalDate birthDay) {
        this.origin = origin;
        this.destination = destination;
        this.weight = weight;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.birthDay = birthDay;
    }

    public Location getOrigin() {
        return origin;
    }

    public Location getDestination() {
        return destination;
    }

    public double getWeight() {
        return weight;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    @Override
    public String toString() {
        return "CargoBookingRequest{" +
                "origin=" + origin +
                ", destination=" + destination +
                ", weight=" + weight +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", birthDay=" + birthDay +
                '}';
    }
}

