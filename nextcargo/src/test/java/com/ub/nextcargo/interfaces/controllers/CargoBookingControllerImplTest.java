package com.ub.nextcargo.interfaces.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.ub.nextcargo.application.CargoBookingService;
import com.ub.nextcargo.domen.*;
import com.ub.nextcargo.exceptions.CargoFieldWasNullException;
import com.ub.nextcargo.exceptions.OriginWasEqualToDestinationException;
import com.ub.nextcargo.interfaces.dto.CargoBookingRequest;
import com.ub.nextcargo.interfaces.dto.CargoDto;
import com.ub.nextcargo.persistance.CargoRepositorySeeker;
import com.ub.nextcargo.util.Location;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class CargoBookingControllerImplTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private CargoRepositorySeeker cargoRepositorySeeker;
    @MockBean
    private CargoBookingService service;

    private Customer customer;
    private LocalDate now;
    private LocalDate in3days;
    private Cargo cargo;
    private CargoBookingRequest cargoRequest;
    private String cargoRequestContentString;
    private ObjectId cargoId;
    private Itinerary invalidItinerary;
    private String invalidItineraryJson;
    private Itinerary validItinerary;
    private String validItineraryJson;
    private String validLocationJson;
    private String invalidLocationJson;
    private String validLocationEqualingToOriginJson;
    private Location location1;
    private ObjectWriter jsonWriter;

    @BeforeEach
    void prepare() throws JsonProcessingException {
        jsonWriter = new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .writer();

        location1 = new Location("A", "B", "C", "D", "E", "F", "G");
        Location location2 = new Location("A1", "B1", "C1", "D1", "E1", "F1", "G1");
        Location location3 = new Location("Turkey", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        Location location4 = new Location("Japan", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        Location location5 = new Location("France", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        Location location6 = new Location("England", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        now = LocalDate.now();
        in3days = now.plusDays(3);

        cargoRequest = new CargoBookingRequest(location1, location2, 100, "Peter", "Zelinskiy", "Konstantinohich", LocalDate.now());

        cargoRequestContentString = jsonWriter.withDefaultPrettyPrinter().writeValueAsString(cargoRequest);
        cargo = new Cargo(cargoRequest.getOrigin(), cargoRequest.getDestination(), now, in3days, cargoRequest.getWeight());

        cargoId = ObjectId.get();
        invalidItinerary = new Itinerary();
        invalidItineraryJson = jsonWriter.withDefaultPrettyPrinter().writeValueAsString(invalidItinerary);
        List<CarrierMovement> carrierMovements = new ArrayList<>();
        carrierMovements.add(new CarrierMovement(location1, location2, LocalDateTime.now(), LocalDateTime.now()));
        carrierMovements.add(new CarrierMovement(location3, location4, LocalDateTime.now(), LocalDateTime.now()));
        Schedule schedule = new Schedule(carrierMovements);
        Voyage voyage = new Voyage("1a", schedule);
        ArrayList<RouteStage> routeStageList = new ArrayList<>();
        routeStageList.add(new RouteStage(voyage, location5, location6, LocalDateTime.now(), LocalDateTime.now()));
        validItinerary = new Itinerary(routeStageList);
        validItineraryJson = jsonWriter.withDefaultPrettyPrinter().writeValueAsString(validItinerary);

        validLocationJson = jsonWriter.withDefaultPrettyPrinter().writeValueAsString(location5);
        invalidLocationJson = jsonWriter.withDefaultPrettyPrinter().writeValueAsString(new Location(null, "b", "c", "d", "e", "f", "j"));
        validLocationEqualingToOriginJson = jsonWriter.withDefaultPrettyPrinter().writeValueAsString(location1);
    }

    @Test
    void bookCargo() throws Exception {
        when(service.bookNewCargo(cargo)).thenReturn(cargo);

        ResultActions resultActions = mvc.perform(post("/api/v1/cargos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(cargoRequestContentString))
                .andExpect(status().isOk());
    }

    @Test
    void chooseItinerary() throws Exception {
        itineraryRequestWithValidParameters();
        itineraryRequestWithInvalidItinerary();
        itineraryRequestWithNotFoundCargo();
        itineraryRequestCantReplaceDocument();
    }

    private void itineraryRequestCantReplaceDocument() throws Exception {
        when(cargoRepositorySeeker.findById(cargoId)).thenReturn(cargo);
        when(cargoRepositorySeeker.replaceDocument(cargoId, cargo)).thenReturn(null);

        ResultActions resultActions = mvc.perform(post("/api/v1/cargos/itineraries/" + cargoId.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(validItineraryJson))
                .andExpect(status().isNoContent());
    }

    private void itineraryRequestWithNotFoundCargo() throws Exception {
        when(cargoRepositorySeeker.findById(cargoId)).thenReturn(null);

        ResultActions resultActions = mvc.perform(post("/api/v1/cargos/itineraries/" + cargoId.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(validItineraryJson))
                .andExpect(status().isNotFound());
    }

    private void itineraryRequestWithValidParameters() throws Exception {
        when(cargoRepositorySeeker.findById(cargoId)).thenReturn(cargo);
        when(cargoRepositorySeeker.replaceDocument(cargoId, cargo)).thenReturn(cargo);

        ResultActions resultActions = mvc.perform(post("/api/v1/cargos/itineraries/" + cargoId.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(validItineraryJson))
                .andExpect(status().isOk());
    }

    void itineraryRequestWithInvalidItinerary() throws Exception {
        when(cargoRepositorySeeker.findById(cargoId)).thenReturn(cargo);

        ResultActions resultActions = mvc.perform(post("/api/v1/cargos/itineraries/" + cargoId.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(invalidItineraryJson))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    void changeDestinationOfCargo() throws Exception {
        changeDestinationWithCorrectParameters();
        changeDestinationNotValidDestination();
        changeDestinationCargoNotFound();
        changeDestinationDestinationWasEqualToOrigin();
        changeDestinationCantUpdateDatabaseDocument();
    }

    private void changeDestinationWithCorrectParameters() throws Exception {
        when(cargoRepositorySeeker.findById(cargoId)).thenReturn(cargo);
        when(cargoRepositorySeeker.replaceDocument(cargoId, cargo)).thenReturn(cargo);

        ResultActions resultActions = mvc.perform(post("/api/v1/cargos/destination/" + cargoId.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(validLocationJson))
                .andExpect(status().isOk());
    }

    private void changeDestinationNotValidDestination() throws Exception {
        ResultActions resultActions = mvc.perform(post("/api/v1/cargos/destination/" + cargoId.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(invalidLocationJson))
                .andExpect(status().isNotAcceptable());
    }

    private void changeDestinationDestinationWasEqualToOrigin() throws Exception {
        when(cargoRepositorySeeker.findById(cargoId)).thenReturn(cargo);
        doThrow(new OriginWasEqualToDestinationException("")).when(service).changeDestination(cargo, location1);

        ResultActions resultActions = mvc.perform(post("/api/v1/cargos/destination/" + cargoId.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(validLocationEqualingToOriginJson))
                .andExpect(status().isNotAcceptable());
    }

    private void changeDestinationCargoNotFound() throws Exception {
        when(cargoRepositorySeeker.findById(cargoId)).thenReturn(null);

        ResultActions resultActions = mvc.perform(post("/api/v1/cargos/destination/" + cargoId.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(validLocationJson))
                .andExpect(status().isNotFound());
    }

    private void changeDestinationCantUpdateDatabaseDocument() throws Exception {
        when(cargoRepositorySeeker.findById(cargoId)).thenReturn(cargo);
        when(cargoRepositorySeeker.replaceDocument(cargoId, cargo)).thenReturn(null);

        ResultActions resultActions = mvc.perform(post("/api/v1/cargos/destination/" + cargoId.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(validLocationJson))
                .andExpect(status().isNotFound());
    }

    @Test
    void getInfoById() throws Exception {
        requestFindCargoByIdIsFound();
        requestFindCargoByIdIsNotFound();
    }

    private void requestFindCargoByIdIsNotFound() throws Exception {
        when(cargoRepositorySeeker.findById(cargoId)).thenReturn(null);

        ResultActions resultActions = mvc.perform(get("/api/v1/cargos/" + cargoId.toString()))
                .andExpect(status().isNotFound());

        Assertions.assertEquals("", resultActions.andReturn().getResponse().getContentAsString());
    }

    private void requestFindCargoByIdIsFound() throws Exception {
        when(cargoRepositorySeeker.findById(cargoId)).thenReturn(cargo);

        ResultActions resultActions = mvc.perform(get("/api/v1/cargos/" + cargoId.toString()))
                .andExpect(status().isOk());

        Assertions.assertEquals(jsonWriter.writeValueAsString(new CargoDto(cargo)), resultActions.andReturn().getResponse().getContentAsString());
    }
}