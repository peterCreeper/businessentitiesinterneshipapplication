package com.ub.nextcargo.application;

import com.ub.nextcargo.domen.*;
import com.ub.nextcargo.persistance.CargoRepository;
import com.ub.nextcargo.util.Location;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

class CargoBookingServiceTest {

    CargoBookingService cargoBookingService;
    @Autowired
    CargoRepository cargoRepository;

    Location location1;
    Location location2;
    Location location3;
    Location location4;
    Location location5;
    Location location6;
    Cargo emptyCargo;
    Cargo validCargoWithoutItinerary;
    Itinerary validItinerary;

    @BeforeEach
    void prepare() {
        cargoBookingService = new CargoBookingServiceImpl(cargoRepository);

        location1 = new Location("Russia", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        location2 = new Location("USA", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        location3 = new Location("Turkey", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        location4 = new Location("Japan", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        location5 = new Location("France", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        location6 = new Location("England", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        emptyCargo = new Cargo();
        validItinerary = getValidItinerary();
        validCargoWithoutItinerary =  new Cargo(location1, location2, LocalDate.now(), LocalDate.now(), 1);
    }

    @Test
    void attachCargoToItinerary() {
        cargoBookingService.attachItineraryToCargo(emptyCargo, validItinerary);
        Assertions.assertEquals(validItinerary, emptyCargo.getItinerary());
    }

    @Test
    void changeDestination() {
        validCargoWithoutItinerary.setNewItinerary(getValidItinerary());

        cargoBookingService.changeDestination(validCargoWithoutItinerary, location4);
        Assertions.assertEquals(location4, validCargoWithoutItinerary.getDestination());
    }

    Itinerary getValidItinerary(){
        List<CarrierMovement> carrierMovements = new ArrayList<>();
        carrierMovements.add(new CarrierMovement(location1, location2, LocalDateTime.now(), LocalDateTime.now()));
        carrierMovements.add(new CarrierMovement(location3, location4, LocalDateTime.now(), LocalDateTime.now()));
        Schedule schedule = new Schedule(carrierMovements);
        Voyage voyage = new Voyage("1a", schedule);

        ArrayList<RouteStage> routeStageList = new ArrayList<>();
        routeStageList.add(new RouteStage(voyage, location5, location6, LocalDateTime.now(), LocalDateTime.now()));

        return new Itinerary(routeStageList);
    }

}