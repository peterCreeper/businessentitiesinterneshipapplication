package com.ub.nextcargo.domen;

import com.ub.nextcargo.exceptions.CargoFieldWasNullException;
import com.ub.nextcargo.exceptions.InvalidCargoWeightException;
import com.ub.nextcargo.exceptions.ItineraryWasNotDefinedException;
import com.ub.nextcargo.exceptions.OriginWasEqualToDestinationException;
import com.ub.nextcargo.util.Location;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

class CargoTest {

    Location location;
    Location location2;
    Location location3;
    Location location4;
    Location location5;
    Location location6;

    List<CarrierMovement> carrierMovements;
    Schedule schedule;
    Voyage voyage;
    ArrayList<RouteStage> routeStageList;

    Cargo validCargo;
    Itinerary validItinerary;
    Cargo cargoWithNotDefinedItinerary;

    @BeforeEach
    void prepare() {
        location = new Location("Russia", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        location2 = new Location("USA", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        location3 = new Location("Turkey", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        location4 = new Location("Japan", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        location5 = new Location("France", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        location6 = new Location("England", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");


        carrierMovements = new ArrayList<>();
        carrierMovements.add(new CarrierMovement(location, location2, LocalDateTime.now(), LocalDateTime.now()));
        carrierMovements.add(new CarrierMovement(location3, location4, LocalDateTime.now(), LocalDateTime.now()));
        schedule = new Schedule(carrierMovements);
        voyage = new Voyage("1a", schedule);

        routeStageList = new ArrayList<>();
        routeStageList.add(new RouteStage(voyage, location5, location6, LocalDateTime.now(), LocalDateTime.now()));

        validCargo = new Cargo(location, location2, LocalDate.now(), LocalDate.now(), 1);
        validItinerary = new Itinerary(routeStageList);
        validCargo.setNewItinerary(validItinerary);

        cargoWithNotDefinedItinerary = new Cargo(location2, location3, LocalDate.now(), LocalDate.now(), 1);
    }

    @Test
    void constructorTesting() {
        checkOriginIsEqualToDestination();
        checkNullFields();
        checkWeight();
    }

    void checkWeight() {
        Assertions.assertThrows(InvalidCargoWeightException.class, () -> {
            Cargo cargo = new Cargo(location, location2, LocalDate.now(), LocalDate.now(), 0);
        });
        Assertions.assertThrows(InvalidCargoWeightException.class, () -> {
            Cargo cargo = new Cargo(location, location2, LocalDate.now(), LocalDate.now(), -1);
        });
    }

    void checkOriginIsEqualToDestination() {
        Assertions.assertThrows(OriginWasEqualToDestinationException.class, () -> {
            Cargo cargo = new Cargo(location, location, LocalDate.now(), LocalDate.now(), 1);
        });
    }

    void checkNullFields() {
        Assertions.assertThrows(CargoFieldWasNullException.class, () -> {
            Cargo cargo = new Cargo(null, location, LocalDate.now(), LocalDate.now(), 1);
        });
        Assertions.assertThrows(CargoFieldWasNullException.class, () -> {
            Cargo cargo = new Cargo(location, null, LocalDate.now(), LocalDate.now(), 1);
        });
        Assertions.assertThrows(CargoFieldWasNullException.class, () -> {
            Cargo cargo = new Cargo(location, location2, null, LocalDate.now(), 1);
        });
        Assertions.assertThrows(CargoFieldWasNullException.class, () -> {
            Cargo cargo = new Cargo(location, location2, LocalDate.now(), null, 1);
        });
    }

    @Test
    void defineNewRoute() {

        checkWhenItineraryWasNotDefined();
        checkWhenItineraryWasDefined();
    }

    void checkWhenItineraryWasNotDefined() {
        Assertions.assertThrows(ItineraryWasNotDefinedException.class, () -> {
            cargoWithNotDefinedItinerary.defineNewRoute(location, location2, LocalDate.now(), LocalDate.now());
        });
    }

    void checkWhenItineraryWasDefined() {
        Assertions.assertThrows(OriginWasEqualToDestinationException.class, () -> {
            validCargo.defineNewRoute(location, location, LocalDate.now(), LocalDate.now());
        });
        Assertions.assertThrows(CargoFieldWasNullException.class, () -> {
            validCargo.defineNewRoute(null, location2, LocalDate.now(), LocalDate.now());
        });
        Assertions.assertThrows(CargoFieldWasNullException.class, () -> {
            validCargo.defineNewRoute(location, null, LocalDate.now(), LocalDate.now());
        });
        Assertions.assertThrows(CargoFieldWasNullException.class, () -> {
            validCargo.defineNewRoute(location, location2, null, LocalDate.now());
        });
        Assertions.assertThrows(CargoFieldWasNullException.class, () -> {
            validCargo.defineNewRoute(location, location2, LocalDate.now(), null);
        });
    }

    @Test
    void setNewItinerary() {
        Assertions.assertThrows(CargoFieldWasNullException.class, () -> {
            cargoWithNotDefinedItinerary.setNewItinerary(null);
        });
    }
}