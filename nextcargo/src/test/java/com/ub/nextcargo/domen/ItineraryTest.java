package com.ub.nextcargo.domen;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ItineraryTest {

    Itinerary emptyItinerary;

    @BeforeEach
    void prepare() {
        emptyItinerary = new Itinerary();
    }

    @Test
    void getRouteStages() {

        assertThrows(UnsupportedOperationException.class, () -> {
            emptyItinerary.getRouteStages().add(null);
        });
    }
}