package com.ub.nextcargo.domen;

import com.ub.nextcargo.exceptions.OriginWasEqualToDestinationException;
import com.ub.nextcargo.util.Location;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DeliveryTest {

    Location location1;
    Location location2;

    Delivery deliveryWithNullItinerary;
    Delivery deliveryWithValidItinerary;
    Itinerary invalidItinerary;
    Itinerary validItinerary;

    @BeforeEach
    void prepare() {
        location1 = new Location("Russia", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        location2 = new Location("USA", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");

        validItinerary = getValidItinerary();
        invalidItinerary = new Itinerary();

        deliveryWithNullItinerary = new Delivery(invalidItinerary, location1, location2);
        deliveryWithValidItinerary = new Delivery(validItinerary, location1, location2);
    }

    @Test
    @DisplayName("Creating delivery")
    void constructor() {

        Assertions.assertThrows(OriginWasEqualToDestinationException.class, () -> {
            Delivery delivery = new Delivery(new Itinerary(), location1, location1);
        });

        Assertions.assertEquals(Delivery.RoutingStatus.ROUTED, deliveryWithNullItinerary.getRoutingStatus());
        Assertions.assertEquals(Delivery.TransportationStatus.NOT_RECEIVED, deliveryWithNullItinerary.getTransportationStatus());
        assertNull(deliveryWithNullItinerary.getCurrentVoyage());
        assertNull(deliveryWithNullItinerary.getLastKnownLocation());


        Assertions.assertEquals(validItinerary.getRouteStages().get(0).getVoyage(), deliveryWithValidItinerary.getCurrentVoyage());
        Assertions.assertEquals(validItinerary.getRouteStages().get(0).getLoadLocation(), deliveryWithValidItinerary.getLastKnownLocation());
    }

    @Test
    void isDetermined() {
        assertFalse(deliveryWithNullItinerary.isDetermined());
    }

    Itinerary getValidItinerary() {
        Location location3 = new Location("Turkey", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        Location location4 = new Location("Japan", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        Location location5 = new Location("France", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");
        Location location6 = new Location("England", "Volgogradskaya", "Volgograd", "Sovetskiy", "Svetskaya", "14A", "12");

        List<CarrierMovement> carrierMovements = new ArrayList<>();
        carrierMovements.add(new CarrierMovement(location1, location2, LocalDateTime.now(), LocalDateTime.now()));
        carrierMovements.add(new CarrierMovement(location3, location4, LocalDateTime.now(), LocalDateTime.now()));
        Schedule schedule = new Schedule(carrierMovements);
        Voyage voyage = new Voyage("1a", schedule);

        ArrayList<RouteStage> routeStageList = new ArrayList<>();
        routeStageList.add(new RouteStage(voyage, location5, location6, LocalDateTime.now(), LocalDateTime.now()));

        return new Itinerary(routeStageList);
    }
}